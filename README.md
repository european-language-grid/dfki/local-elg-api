local-elg-api
=============

Locally running proxy to the ELG Catalogue Search API.

This is useful for developing Web applications that consume this API. When
running such applications on the developer's local machine, Web applications
cannot directly access the API, browsers will block it due to the Cross Origin
Resource Sharing (CORS) policy.

To solve this, run this little helper in the background. It will proxy the API
and expose it at `http://localhost:5000/catalogue_backend/api/registry/search/`
with a liberal CORS policy.

Set it up with this command:

    npm install

Run it with this command:

    npm start
