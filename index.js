const express = require('express');
const request = require('request');

const ORIGIN = 'https://live.european-language-grid.eu'
const PATH = '/catalogue_backend/api/registry/search/';
const REMOTE_PREFIX = `${ORIGIN}${PATH}`;
const PORT = process.env.PORT || 5000;
const LOCAL_PREFIX = `http://localhost:${PORT}/catalogue_backend/api/registry/search/`;

const app = express();

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  next();
});

app.get(PATH, (req, res) => {
  const params = new URLSearchParams();
  for (var key in req.query) {
    params.append(key, req.query[key]);
  }
  request(
    { url: REMOTE_PREFIX + '?' + params.toString() },
    (error, response, body) => {
      if (error || response.statusCode !== 200) {
        return res.status(500).json({ type: 'error', message: err.message });
      }

      const json = JSON.parse(body);
      if (json.previous !== null) {
        json.previous = json.previous.replace(REMOTE_PREFIX, LOCAL_PREFIX);
      }
      if (json.next !== null) {
        json.next = json.next.replace(REMOTE_PREFIX, LOCAL_PREFIX);
      }
      res.json(json);
    }
  )
});

app.listen(PORT, () => console.log(`listening on ${PORT}`));
